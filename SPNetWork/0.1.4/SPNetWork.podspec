Pod::Spec.new do |spec|
#项目名称
spec.name         = 'SPNetWork'
#版本号
spec.version      = '0.1.4'
#开源协议
spec.license      = 'MIT'
#对开源项目的描述
spec.summary      = 'SPNetWork by remantou!'
#开源项目的首页
spec.homepage     = 'https://gitlab.com/remantou/SPNetWork'
#作者信息
spec.author       = {'remantou' => 'Leung_1988@126.com'}
#项目的源和版本号
spec.source       = { :git => 'https://gitlab.com/remantou/SPNetWork.git', :tag => '0.1.4' }
#源文件，这个就是供第三方使用的源文件
spec.source_files = 'SPNetwork/*'
#适用于ios7及以上版本
spec.platform     = :ios, '8.0'
#使用的是ARC
spec.requires_arc = true
end
